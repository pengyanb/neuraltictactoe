const debug = require("debug")("NN:TicTacToeGame");
const boardSize = 9;
// board = 0b 00 00 00 00 00 00 00 00 00
// 2 bits * 9,
// for each 2 bits block,
// lower bit = 0 means slot is empty, bit = 1 means slot is occupied
// higher bit = 0 means slot occupied by O, bit = 1 means slot occupied by X
// [Slot orientation, board lower bit to hight bit]
// 1, 2, 3,
// 4, 5, 6,
// 7, 8, 9
const xWinningPattern = [
  0b000000000000111111, //X, X, X,   _, _, _,  _, _, _
  0b000000111111000000, //_, _, _,   X, X, X,  _, _, _
  0b111111000000000000, //_, _, _,   _, _, _,  X, X, X
  0b000011000011000011, //X, _, _,   X, _, _,  X, _, _
  0b001100001100001100, //_, X, _,   _, X, _,  _, X, _
  0b110000110000110000, //_, _, X,   _, _, X,  _, _, X
  0b110000001100000011, //X, _, _,   _, X, _,  _, _, X
  0b000011001100110000 //_, _, X,   _, X, _,  X, _, _
];

const winningPatterMask = [
  0b000000000000111111, //X, X, X,   _, _, _,  _, _, _
  0b000000111111000000, //_, _, _,   X, X, X,  _, _, _
  0b111111000000000000, //_, _, _,   _, _, _,  X, X, X
  0b000011000011000011, //X, _, _,   X, _, _,  X, _, _
  0b001100001100001100, //_, X, _,   _, X, _,  _, X, _
  0b110000110000110000, //_, _, X,   _, _, X,  _, _, X
  0b110000001100000011, //X, _, _,   _, X, _,  _, _, X
  0b000011001100110000 //_, _, X,   _, X, _,  X, _, _
];

const oWinningPattern = [
  0b000000000000010101, //O, O, O,   _, _, _,  _, _, _
  0b000000010101000000, //_, _, _,   O, O, O,  _, _, _
  0b010101000000000000, //_, _, _,   _, _, _,  O, O, O
  0b000001000001000001, //O, _, _,   O, _, _,  O, _, _
  0b000100000100000100, //_, O, _,   _, O, _,  _, O, _
  0b010000010000010000, //_, _, O,   _, _, O,  _, _, O
  0b010000000100000001, //O, _, _,   _, O, _,  _, _, O
  0b000001000100010000 //_, _, O,   _, O, _,  O, _, _
];

const noWinnerPattern = 0b010101010101010101;

export default class TicTacToeGame {
  constructor(initialTurn = "X") {
    this.board = 0b0;
    this.winner = "";
    this.moveCount = 0;
    if (initialTurn === "X") {
      this.turn = initialTurn; //X's turn
    } else {
      this.turn = "O";
    }
  }

  placeAtSlot(slotIndex) {
    if (this.winner !== "") {
      //if there is a winner already
      debug("[winner]: ", this.winner);
      return false;
    }
    let slotStatus = (this.board >> (slotIndex * 2)) & 0b11;
    //debug("[slotStatus]: ", slotStatus);
    if (slotStatus !== 0) {
      //if this slot is occupied already
      return false;
    }
    let bitBlock = 0b01;
    let mask = ~(0b11 << (slotIndex * 2));
    if (this.turn === "X") {
      bitBlock = 0b11;
      this.turn = "O"; //next turn
    } else {
      this.turn = "X"; //next turn
    }
    bitBlock = bitBlock << (slotIndex * 2);
    this.board = (this.board & mask) | bitBlock;
    debug("[this.board]: ", this.board.toString(2));
    this._checkForWinner();
    this.moveCount++;
    return true;
  }

  getSlotStatus(slotIndex) {
    let slotStatus = (this.board >> (slotIndex * 2)) & 0b11;
    //debug("[getSlotStatus]: ", slotIndex, slotStatus);
    if (slotStatus === 0b00) {
      return "";
    } else if (slotStatus === 0b11) {
      return "X";
    } else if (slotStatus === 0b01) {
      return "O";
    }
  }

  evalueateBoardWinner(board) {
    let winner = "_";
    for (let i = 0; i < winningPatterMask.length; i++) {
      let patternX = xWinningPattern[i];
      let patternO = oWinningPattern[i];
      let mask = winningPatterMask[i];
      let result = board & mask;
      if (result === patternX) {
        winner = "X";
        break;
      } else if (result === patternO) {
        winner = "O";
        break;
      }
    }
    return winner;
  }

  evalueateMoveNew(board, symbol, winners) {
    let emptySlotIndexs = this.getEmptySlotIndexs(board);
    let bitBlock = 0b01; // symbol O
    if (symbol === "X") {
      bitBlock = 0b11;
    }
    if (emptySlotIndexs.length > 0) {
      //console.log("[emptySlotIndexs]: ", emptySlotIndexs);
      for (let i = 0; i < emptySlotIndexs.length; i++) {
        let emptySlotIndex = emptySlotIndexs[i];
        let mask = ~(0b11 << (emptySlotIndex * 2));
        let bitBlockOnBoard = bitBlock << (emptySlotIndex * 2);
        let newBoard = (board & mask) | bitBlockOnBoard;
        // console.log(
        //   "[emptySlotIndex]: ",
        //   emptySlotIndex,
        //   board.toString(2),
        //   newBoard.toString(2)
        // );
        let winner = this.evalueateBoardWinner(newBoard);
        //console.log("[winner]: ", winner);
        if (winner !== "_") {
          winners.push(winner);
          break;
        } else {
          winners = this.evalueateMoveNew(
            newBoard,
            symbol === "X" ? "O" : "X",
            winners
          );
        }
      }
    } else {
      let winner = this.evalueateBoardWinner(board);
      winners.push(winner);
    }
    return winners;
  }

  evalueateMove(board, slotIndex, symbol, recursiveIndex, recursiveLevelCount) {
    // return possiblity of winning
    let slotStatus = (board >> (slotIndex * 2)) & 0b11;
    if (slotStatus !== 0 && recursiveIndex === 0) {
      return 0;
    }
    let bitBlock = 0b01;
    let mask = ~(0b11 << (slotIndex * 2));
    if (symbol === "X") {
      bitBlock = 0b11;
    }
    bitBlock = bitBlock << (slotIndex * 2);
    board = (board & mask) | bitBlock;

    let winnerSymbol = "";
    for (let i = 0; i < winningPatterMask.length; i++) {
      let patternX = xWinningPattern[i];
      let patternO = oWinningPattern[i];
      let mask = winningPatterMask[i];
      let result = board & mask;
      if (result === patternX) {
        winnerSymbol = "X";
        break;
      } else if (result === patternO) {
        winnerSymbol = "O";
        break;
      }
    }
    if (winnerSymbol !== "") {
      return 1 - recursiveIndex * 0.1;
    } else if (recursiveIndex < recursiveLevelCount) {
      let opponentSymbol = symbol === "X" ? "O" : "X";
      let emptySlotIndexs = this.getEmptySlotIndexs(board);
      if (emptySlotIndexs.length > 0) {
        let maxWeight = 0;
        for (let i = 0; i < emptySlotIndexs.length; i++) {
          recursiveIndex++;
          let winningRate = this.evalueateMove(
            board,
            emptySlotIndexs[i],
            opponentSymbol,
            recursiveIndex,
            recursiveLevelCount
          );
          maxWeight = Math.max(winningRate, winningRate);
        }
        return maxWeight;
      } else {
        //draw, no winner, no empty slot
        return 0.6;
      }
    } else {
      return 0.5;
    }
  }

  getEmptySlotIndexs(board) {
    if (typeof board === typeof undefined) {
      board = this.board;
    }
    //console.log("[board]: ", board.toString(2));
    let indexs = [];
    for (let i = 0; i < boardSize; i++) {
      //console.log("slot: ", i, board >> (i * 2));
      //console.log("[getEmptySlotIndex] board: ", board.toString(2));
      if (((board >> (i * 2)) & 0b000000000000000001) === 0) {
        indexs.push(i);
      }
    }
    return indexs;
  }

  _checkForWinner() {
    for (let i = 0; i < winningPatterMask.length; i++) {
      let patternX = xWinningPattern[i];
      let patternO = oWinningPattern[i];
      let mask = winningPatterMask[i];
      let result = this.board & mask;
      if (result === patternX) {
        this.winner = "X";
        break;
      } else if (result === patternO) {
        this.winner = "O";
        break;
      }
    }
    if (this.winner === "") {
      if ((this.board & noWinnerPattern) === noWinnerPattern) {
        this.winner = "_";
      }
    }
  }
}
