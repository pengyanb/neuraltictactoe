import _ from "lodash";
import Perceptron from "./Perceptron";

// const debug = require("debug")("NN:NeuralNetwork");

export default class NeuralNetworkAI {
  constructor(
    playerSymbol,
    inputsCount,
    outputsCount,
    hiddenLayerPerceptronsCount,
    neuralNetWeights
  ) {
    this.playerSymbol = playerSymbol;
    this.inputsCount = inputsCount;
    this.outputsCount = outputsCount;
    this.hiddenLayerPerceptronsCount = hiddenLayerPerceptronsCount;

    this.hiddenLayerPerceptrons = [];
    this.outputLayerPerceptrons = [];
    this.Etotal = 0;
    this.backPropagationThreshold = 0.1;
    this.bufferedWeights = null;

    if (neuralNetWeights) {
      let hiddenLayersWeights = neuralNetWeights["hiddenLayers"];
      for (let i = 0; i < this.hiddenLayerPerceptronsCount; i++) {
        this.hiddenLayerPerceptrons.push(
          new Perceptron({
            weights: hiddenLayersWeights[i],
            id: `HiddenLayerPerceptron${i}`
          })
        );
      }
      let outputLayersWeights = neuralNetWeights["outputLayers"];
      for (let i = 0; i < this.outputsCount; i++) {
        this.outputLayerPerceptrons.push(
          new Perceptron({
            weights: outputLayersWeights[i],
            id: `OutputLayerPerceptron${i}`
          })
        );
      }
    } else {
      for (let i = 0; i < this.hiddenLayerPerceptronsCount; i++) {
        this.hiddenLayerPerceptrons.push(
          new Perceptron({
            inputsCount: this.inputsCount,
            outputsCount: this.outputsCount,
            id: `HiddenLayerPerceptron${i}`
          })
        );
      }
      for (let i = 0; i < this.outputsCount; i++) {
        this.outputLayerPerceptrons.push(
          new Perceptron({
            inputsCount: this.hiddenLayerPerceptronsCount,
            outputsCount: 1,
            id: `OutputLayerPerceptron${i}`
          })
        );
      }
    }
  }

  play(ticTacToeGame) {
    // [input = 1, slot occupied by self]
    // [input = 0, slot is empty]
    // [input = -1, slot is occupied by oppornent]
    let validMove = false;
    let invalidCount = 0;
    let nextMoveIndex = NaN;

    while (!validMove && ticTacToeGame.winner === "") {
      let inputs = [];
      let outputs = [];
      for (let i = 0; i < this.inputsCount; i++) {
        let slotStatus = ticTacToeGame.getSlotStatus(i);
        if (slotStatus === "") {
          inputs.push(0);
        } else if (slotStatus === this.playerSymbol) {
          inputs.push(1);
        } else {
          inputs.push(-1);
        }
      }
      //inputs = [0.05, 0.1]; //!!
      let hiddenLayerOutputs = [];
      for (let i = 0; i < this.hiddenLayerPerceptrons.length; i++) {
        hiddenLayerOutputs.push(
          this.hiddenLayerPerceptrons[i].getOutput(inputs)
        );
      }
      //console.log("[hiddenLayerOutputs]: ", hiddenLayerOutputs);

      for (let i = 0; i < this.outputLayerPerceptrons.length; i++) {
        outputs.push(
          Number(
            this.outputLayerPerceptrons[i]
              .getOutput(hiddenLayerOutputs)
              .toFixed(4)
          )
        );
      }
      //console.log("[outputs]: ", outputs);
      let emptySlots = ticTacToeGame.getEmptySlotIndexs();
      let winnerMove = null;
      for (let i = 0; i < emptySlots.length; i++) {
        let emptySlotIndex = emptySlots[i];
        let xBitBlock = 0b11;
        let oBitBlock = 0b01;
        let mask = ~(0b11 << (emptySlotIndex * 2));
        let xBitBlockOnBoard = xBitBlock << (emptySlotIndex * 2);
        let oBitBlockOnBoard = oBitBlock << (emptySlotIndex * 2);
        let xNewBoard = (ticTacToeGame.board & mask) | xBitBlockOnBoard;
        let oNewBoard = (ticTacToeGame.board & mask) | oBitBlockOnBoard;
        if (
          ticTacToeGame.evalueateBoardWinner(xNewBoard) !== "_" ||
          ticTacToeGame.evalueateBoardWinner(oNewBoard) !== "_"
        ) {
          winnerMove = emptySlotIndex;
          break;
        }
      }
      nextMoveIndex = -1;
      if (winnerMove) {
        console.log("[winnerMove]: ", winnerMove);
        nextMoveIndex = winnerMove;
      } else {
        let nextCandidateMoves = [];
        let maxValue = -1;
        outputs.map((output, index) => {
          let isEmptySlot = false;
          for (let i = 0; i < emptySlots.length; i++) {
            if (emptySlots[i] === index) {
              isEmptySlot = true;
              break;
            }
          }
          if (isEmptySlot) {
            if (output > maxValue) {
              maxValue = output;
              nextCandidateMoves = [index];
            } else if (output === maxValue) {
              nextCandidateMoves.push(index);
            }
          }
        });
        nextMoveIndex =
          nextCandidateMoves[
            Math.floor(Math.random() * nextCandidateMoves.length)
          ];

        console.log(
          `[${this.playerSymbol} nextMove Index]: `,
          nextMoveIndex,
          outputs,
          nextCandidateMoves
        );
      }
      validMove = ticTacToeGame.placeAtSlot(nextMoveIndex);
      invalidCount++;
      if (invalidCount > 10) {
        throw new Error("Too many invalid result");
      }
    }
    return nextMoveIndex;
  }

  train(ticTacToeGame) {
    // [input = 1, slot occupied by self]
    // [input = 0, slot is empty]
    // [input = -1, slot is occupied by oppornent]
    let validMove = false;
    let invalidCount = 0;
    while (!validMove && ticTacToeGame.winner === "") {
      let inputs = [];
      let outputs = [];
      for (let i = 0; i < this.inputsCount; i++) {
        let slotStatus = ticTacToeGame.getSlotStatus(i);
        if (slotStatus === "") {
          inputs.push(0);
        } else if (slotStatus === this.playerSymbol) {
          inputs.push(1);
        } else {
          inputs.push(-1);
        }
      }
      //inputs = [0.05, 0.1]; //!!
      let hiddenLayerOutputs = [];
      for (let i = 0; i < this.hiddenLayerPerceptrons.length; i++) {
        hiddenLayerOutputs.push(
          this.hiddenLayerPerceptrons[i].getOutput(inputs)
        );
      }
      //console.log("[hiddenLayerOutputs]: ", hiddenLayerOutputs);

      for (let i = 0; i < this.outputLayerPerceptrons.length; i++) {
        outputs.push(
          Number(
            this.outputLayerPerceptrons[i]
              .getOutput(hiddenLayerOutputs)
              .toFixed(4)
          )
        );
      }
      //console.log("[outputs]: ", outputs);

      let bitBlock = 0b01;
      let opponentBitBlock = 0b11;
      let oppornentSymbol = "X";
      if (this.playerSymbol === "X") {
        bitBlock = 0b11;
        opponentBitBlock = 0b01;
        oppornentSymbol = "O";
      }
      let outTargets = [];
      let newBoard = 0;

      let emptySlots = ticTacToeGame.getEmptySlotIndexs();
      let winnerMove = null;
      let opponentWinner = null;
      for (let i = 0; i < emptySlots.length; i++) {
        let emptySlotIndex = emptySlots[i];
        let mask = ~(0b11 << (emptySlotIndex * 2));
        let bitBlockOnBoard = bitBlock << (emptySlotIndex * 2);
        let opponentBitBlockOnBoard = opponentBitBlock << (emptySlotIndex * 2);
        newBoard = (ticTacToeGame.board & mask) | bitBlockOnBoard;
        let opponentNewBoard =
          (ticTacToeGame.board & mask) | opponentBitBlockOnBoard;
        if (ticTacToeGame.evalueateBoardWinner(newBoard) !== "_") {
          winnerMove = emptySlotIndex;
        } else if (
          ticTacToeGame.evalueateBoardWinner(opponentNewBoard) !== "_"
        ) {
          opponentWinner = emptySlotIndex;
        }
      }
      if (emptySlots.length === 9) {
        outTargets = [0, 0, 0, 0, 1, 0, 0, 0, 0];
      } else if (winnerMove !== null || opponentWinner !== null) {
        for (let i = 0; i < this.outputsCount; i++) {
          if (i === winnerMove) {
            outTargets.push(1);
          } else if (i === opponentWinner) {
            outTargets.push(0.9);
          } else {
            outTargets.push(0);
          }
        }
      } else {
        let winners = [];
        let opponentWinners = [];
        for (let i = 0; i < outputs.length; i++) {
          let slotStatus = ticTacToeGame.getSlotStatus(i);
          if (slotStatus === "") {
            let mask = ~(0b11 << (i * 2));
            let bitBlockOnBoard = bitBlock << (i * 2);
            let opponentBitBlockOnBoard = opponentBitBlock << (i * 2);
            let playerNewBoard = (ticTacToeGame.board & mask) | bitBlockOnBoard;
            let opponentNewBoard =
              (ticTacToeGame.board & mask) | opponentBitBlockOnBoard;

            winners = ticTacToeGame.evalueateMoveNew(
              playerNewBoard,
              oppornentSymbol,
              []
            );
            opponentWinners = ticTacToeGame.evalueateMoveNew(
              opponentNewBoard,
              this.playerSymbol,
              []
            );

            let winCount = 0;
            let opponentWinCount = 0;
            winners.map(winner => {
              if (winner === this.playerSymbol) {
                winCount += 2;
              } else if (winner === "_") {
                winCount += 1;
              } else {
                winCount += 0.1;
              }
            });
            opponentWinners.map(winner => {
              if (winner === oppornentSymbol) {
                opponentWinCount += 2;
              } else if (winner === "_") {
                opponentWinCount += 1;
              } else {
                opponentWinCount += 0.1;
              }
            });

            let outTarget = Math.max(
              Number((winCount / (winners.length * 2)).toFixed(4)),
              Number(
                (opponentWinCount / (opponentWinners.length * 2)).toFixed(4)
              )
            );

            outTargets.push(outTarget);
          } else {
            outTargets.push(0);
          }
        }
      }
      //outTargets = [0.01, 0.99]; //!!

      //console.log("[outTargets]: ", outTargets);
      let validTargets = false;
      outTargets.map(t => {
        if (t > 0) {
          validTargets = true;
        }
      });
      if (!validTargets) {
        throw new Error("Invalid output targets");
      }

      let Etotal = 0;
      for (let i = 0; i < outputs.length; i++) {
        Etotal += 0.5 * Math.pow(outTargets[i] - outputs[i], 2);
      }
      //console.log("[Etotal]: ", Etotal);
      this.Etotal = Etotal;
      // dEtotal / dw1 = - (target - out) * out * (1 - out) * out_h1

      let thetas = [];
      let newOutputLayersWeightsArray = [];

      for (let i = 0; i < this.outputLayerPerceptrons.length; i++) {
        let out = outputs[i];
        let target = outTargets[i];
        let outputLayerPerceptron = this.outputLayerPerceptrons[i];
        let theta = NaN;
        let learningRate = outputLayerPerceptron.learningRate;
        let newWeights = outputLayerPerceptron.weights.map((weight, index) => {
          let outputLayerPerceptronInput = outputLayerPerceptron.inputs[index];
          let newWeight = NaN;

          theta = (out - target) * out * (1 - out);
          if (index === outputLayerPerceptron.weights.length - 1) {
            newWeight = weight - learningRate * theta;
          } else {
            newWeight =
              weight - learningRate * theta * outputLayerPerceptronInput;
          }

          return newWeight;
        });
        //console.log(`[${outputLayerPerceptron.id} newWeights]:`, newWeights);
        //outputLayerPerceptron.weights = newWeights;
        thetas.push(theta);
        newOutputLayersWeightsArray.push(newWeights);
      }
      //console.log("[Thetas]: ", thetas);
      //let newHiddenLayersWeightsArray = [];

      for (let i = 0; i < this.hiddenLayerPerceptrons.length; i++) {
        let hiddenLayerPerceptron = this.hiddenLayerPerceptrons[i];
        let outputLayerPerceptron = this.hiddenLayerPerceptrons[i];
        let out_h = hiddenLayerPerceptron.output;
        let learningRate = hiddenLayerPerceptron.learningRate;
        //console.log("[hiddenLayer weights]: ", hiddenLayerPerceptron.weights);
        let newWeights = hiddenLayerPerceptron.weights.map((weight, index) => {
          let input = inputs[index];
          let newWeight = NaN;
          let theta_times_Wh_sum = 0;
          thetas.map((theta, j) => {
            let Wh = outputLayerPerceptron.weights[j];
            theta_times_Wh_sum += theta * Wh;
          });
          if (index === hiddenLayerPerceptron.weights.length - 1) {
            newWeight =
              weight - learningRate * theta_times_Wh_sum * out_h * (1 - out_h);
          } else {
            newWeight =
              weight -
              learningRate * theta_times_Wh_sum * out_h * (1 - out_h) * input;
          }
          return newWeight;
        });
        //console.log("[hiddenLayer newWeights]: ", newWeights);
        //newHiddenLayersWeightsArray.push(newWeights);
        hiddenLayerPerceptron.weights = newWeights;
      }

      for (let i = 0; i < this.outputLayerPerceptrons.length; i++) {
        this.outputLayerPerceptrons[i].weights = newOutputLayersWeightsArray[i];
      }

      let nextMoveIndex = -1;
      let nextCandidateMoves = [];
      let maxValue = -1;
      outputs.map((output, index) => {
        if (output > maxValue) {
          maxValue = output;
          nextCandidateMoves = [index];
        } else if (output === maxValue) {
          nextCandidateMoves.push(index);
        }
      });

      nextMoveIndex =
        nextCandidateMoves[
          Math.floor(Math.random() * nextCandidateMoves.length)
        ];

      //console.log(`[${this.playerSymbol} nextMove Index]: `, nextMoveIndex);
      // console.log("[board before move]: ", ticTacToeGame.board.toString(2));
      validMove = ticTacToeGame.placeAtSlot(nextMoveIndex);
      // if (validMove) {
      //   if (Etotal > 0.2) {
      //     validMove = false;
      //   }
      // }
      // console.log("[validMove]: ", validMove);
      if (validMove) {
        if (invalidCount !== 0) {
          console.log("[Invalid Count]: ", invalidCount);
        }
        invalidCount = 0;
      }
      invalidCount++;
      if (invalidCount > 10000) {
        console.log("[invalidCount]: ", invalidCount);
        console.log("[outputs]: ", outputs);
        console.log("[Targets]: ", outTargets);
        console.log(`[nextMoveIndex] ${this.playerSymbol} ${nextMoveIndex}`);
        console.log("[board]: ", ticTacToeGame.board.toString(2));
        console.log("[nextCandidateMoves]: ", nextCandidateMoves);
        throw new Error("Too many invalid result");
      }
    }
    return ticTacToeGame;
  }

  getNeuralNetworkWeightsInfo() {
    let nnWeightsInfo = { hiddenLayers: [], outputLayers: [] };
    for (let i = 0; i < this.hiddenLayerPerceptronsCount; i++) {
      nnWeightsInfo.hiddenLayers.push(this.hiddenLayerPerceptrons[i].weights);
    }
    for (let i = 0; i < this.outputsCount; i++) {
      nnWeightsInfo.outputLayers.push(this.outputLayerPerceptrons[i].weights);
    }
    return nnWeightsInfo;
  }

  bufferWeights() {
    this.bufferedWeights = _.cloneDeep(this.getNeuralNetworkWeightsInfo());
  }
  clearBufferedWeights() {
    this.bufferedWeights = null;
  }
  restoreFromBufferedWeights() {
    if (this.bufferedWeights) {
      console.log("[restoreFromBufferedWeights]: ", this.playerSymbol);
      let hiddenLayers = this.bufferedWeights.hiddenLayers;
      let outputLayers = this.bufferedWeights.outputLayers;
      for (let i = 0; i < this.hiddenLayerPerceptronsCount; i++) {
        this.hiddenLayerPerceptrons[i].weights = hiddenLayers[i];
      }
      for (let i = 0; i < this.outputsCount; i++) {
        this.outputLayerPerceptrons[i].weights = outputLayers[i];
      }
    }
  }
}
