export default class Perceptron {
  constructor(options) {
    //options.inputsCount, options.outputsCount || options.weights
    this.id = "Perceptron";
    this.learningRate = 0.1;
    this.inputsCount = 2;
    this.weights = []; //[...inputs weights, bias weight]
    this.inputs = [];
    this.netValue = 0;
    this.output = 0;

    if (options.id) {
      this.id = options.id;
    }
    if (options.inputsCount) {
      this.inputsCount = options.inputsCount;
    }

    if (options.weights) {
      if (options.weights.length > 0) {
        this.weights = options.weights;
        this.inputsCount = this.weights.length - 1;
      } else {
        throw new Error("Perception constructor received invalid weights");
      }
    } else {
      for (let j = 0; j < this.inputsCount; j++) {
        this.weights.push(Math.random() * 2 - 1); //inputs weights
      }
      this.weights.push(Math.random() * 2 - 1); //bias weights
    }
    // console.log(this.id);
    // console.log("[Perceptron inputsCount]: ", this.inputsCount);
    //console.log("[Perceptron weights]: ", this.weights);
  }

  getOutput(inputs) {
    let sum = 0;
    this.inputs = inputs;
    //console.log("[this.inputs]: ", this.inputs);
    //console.log("[this.weights]: ", this.weights);
    for (let i = 0; i < this.inputsCount; i++) {
      //console.log(`[${this.id} Inputs]: `, inputs);
      //console.log(`[${this.id} Weights]: `, this.weights);
      if (inputs.length + 1 !== this.weights.length) {
        throw new Error("Perceptron received invalid inputs");
      }
      //console.log(`${inputs[i]} * ${this.weights[i]}`);
      sum += inputs[i] * this.weights[i];
    }
    sum += this.weights[this.weights.length - 1]; // bias weight
    this.netValue = sum;
    let output = this.activationFunc(sum);
    this.output = output;
    return output;
  }

  // train(inputs, targets) {
  //   let guessedOutputs = this.getOutputs(inputs);
  //   if (guessedOutputs.length !== targets.length) {
  //     throw new Error(
  //       "Train process targets length not match to guessedOuputs length"
  //     );
  //   }
  //   for (let i = 0; i < guessedOutputs.length; i++) {
  //     let error = targets[i] - guessedOutputs[i];
  //     for (let j = 0; j < this.weights[i].length; j++) {
  //       this.weights[i][j] = error * inputs[i] * this.learningRate;
  //     }
  //   }
  // }

  activationFunc(x) {
    return 1 / (1 + Math.exp(-1 * x)); // 1 / (1 + e^x)
  }
}
