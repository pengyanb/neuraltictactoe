import React, { Component } from "react";
import { Layout, Menu, Row, Col, Button, Input, Dropdown } from "antd";
import { Stage, Layer, Line, Circle, Rect } from "react-konva";
import "./App.css";

import TicTacToeGame from "./NN/TicTacToeGame";
import NeuralNetworkAI from "./NN/NeuralNetworkAI";
import TrainedWeights from "./NN/TrainedWeights";

const { Header, Content } = Layout;

const boardSize = 9;
const boardWidth = 180;
const boardHeight = 180;
const boardRowCount = 3;
const boardColumnCount = 3;
const slotWidth = boardWidth / boardColumnCount;
const slotHeight = boardHeight / boardRowCount;
const halfSlotWidth = slotWidth / 2;
const halfSlotHeight = slotHeight / 2;
const quaterSlotWidth = halfSlotWidth / 2;
const quaterSlotHeight = halfSlotHeight / 2;

const PlayerXSymbol = "X";
const playerOSymbol = "O";

const neuralNetworkInputsCount = 9;
const neuralNetworkOutputsCount = 9;
const neuralNetworkHiddenLayersCount = 48;

class App extends Component {
  constructor(props) {
    super(props);

    let boardCount = 1;
    let boards = [];
    let trainingFlags = [];
    let playerXnnAis = [];
    let playerOnnAis = [];
    let boardsStatus = [];
    let trainingGameCount = [];
    let playerXWinningCount = [];
    let playerOWinningCount = [];
    let drawCount = [];
    let xETotals = [];
    let oETotals = [];
    for (let i = 0; i < boardCount; i++) {
      boards[i] = new TicTacToeGame();
      playerXnnAis[i] = new NeuralNetworkAI(
        PlayerXSymbol,
        neuralNetworkInputsCount,
        neuralNetworkOutputsCount,
        neuralNetworkHiddenLayersCount,
        TrainedWeights.xWeights
      );
      playerOnnAis[i] = new NeuralNetworkAI(
        playerOSymbol,
        neuralNetworkInputsCount,
        neuralNetworkOutputsCount,
        neuralNetworkHiddenLayersCount,
        TrainedWeights.oWeights
      );
      let boardStatus = [];
      for (let j = 0; j < boardSize; j++) {
        boardStatus.push("");
      }
      boardsStatus[i] = boardStatus;
      trainingFlags[i] = false;
      trainingGameCount[i] = 0;
      playerXWinningCount[i] = 0;
      playerOWinningCount[i] = 0;
      drawCount[i] = 0;
      xETotals[i] = 0;
      oETotals[i] = 0;
    }

    let gameBoardStatus = [];
    for (let j = 0; j < boardSize; j++) {
      gameBoardStatus.push("");
    }

    this.state = {
      selectedMenuKey: "train",
      boardCount: 1,
      boards: boards,
      trainingFlags: trainingFlags,
      boardsStatus: boardsStatus,
      playerXnnAis: playerXnnAis,
      playerOnnAis: playerOnnAis,
      trainingGameCount: trainingGameCount,
      playerXWinningCount: playerXWinningCount,
      playerOWinningCount: playerOWinningCount,
      drawCount: drawCount,
      xETotals: xETotals,
      oETotals: oETotals,
      gameBoard: new TicTacToeGame(),
      gameBoardStatus: gameBoardStatus,
      gameBoardnnAi: new NeuralNetworkAI(
        PlayerXSymbol,
        neuralNetworkInputsCount,
        neuralNetworkOutputsCount,
        neuralNetworkHiddenLayersCount,
        TrainedWeights.xWeights
      ),
      gameBoardnnAiSymbol: "X",
      gameBoardWeights: TrainedWeights.xWeights,
      gameBoardStarted: false
    };
    this.trainIntervalTimer = null;
  }

  onStartButtonClick = index => {
    let boards = this.state.boards;
    let playerXnnAis = this.state.playerXnnAis;
    let playerOnnAis = this.state.playerOnnAis;
    let boardsStatus = this.state.boardsStatus;
    let trainingFlags = this.state.trainingFlags;
    let trainingGameCount = this.state.trainingGameCount;
    let playerXWinningCount = this.state.playerXWinningCount;
    let playerOWinningCount = this.state.playerOWinningCount;
    let drawCount = this.state.drawCount;
    let xETotals = this.state.xETotals;
    let oETotals = this.state.oETotals;

    trainingFlags[index] = false;
    trainingGameCount[index] = 0;
    playerXWinningCount[index] = 0;
    playerOWinningCount[index] = 0;
    drawCount[index] = 0;
    xETotals[index] = 0;
    oETotals[index] = 0;
    boards[index] = new TicTacToeGame();

    playerXnnAis[index] = new NeuralNetworkAI(
      PlayerXSymbol,
      neuralNetworkInputsCount,
      neuralNetworkOutputsCount,
      neuralNetworkHiddenLayersCount
    );

    playerOnnAis[index] = new NeuralNetworkAI(
      playerOSymbol,
      neuralNetworkInputsCount,
      neuralNetworkOutputsCount,
      neuralNetworkHiddenLayersCount
    );
    //!!
    // let nerualNetWeights = {
    //   hiddenLayers: [[0.15, 0.2, 0.35], [0.25, 0.3, 0.35]],
    //   outputLayers: [[0.4, 0.45, 0.6], [0.5, 0.55, 0.6]]
    // };
    // playerXnnAis[index] = new NeuralNetworkAI(
    //   PlayerXSymbol,
    //   2,
    //   2,
    //   2,
    //   nerualNetWeights
    // );
    for (let j = 0; j < boardSize; j++) {
      boardsStatus[index][j] = "";
    }
    this.setState({
      boards: boards,
      boardsStatus: boardsStatus,
      playerXnnAis: playerXnnAis,
      playerOnnAis: playerOnnAis,
      trainingFlags: trainingFlags,
      trainingGameCount: trainingGameCount,
      playerXWinningCount: playerXWinningCount,
      playerOWinningCount: playerOWinningCount,
      drawCount: drawCount,
      xETotals: xETotals,
      oETotals: oETotals
    });
  };

  onTrainButtonClick = (index, flag) => {
    let boards = this.state.boards;
    let boardsStatus = this.state.boardsStatus;
    boards[index] = new TicTacToeGame();
    for (let j = 0; j < boardSize; j++) {
      boardsStatus[index][j] = "";
    }
    let trainingFlags = this.state.trainingFlags;
    if (flag) {
      trainingFlags[index] = flag;
    } else {
      trainingFlags[index] = !trainingFlags[index];
      if (this.trainIntervalTimer !== null) {
        clearTimeout(this.trainIntervalTimer);
        this.trainIntervalTimer = null;
      }
    }
    this.setState(
      {
        trainingFlags: trainingFlags,
        boards: boards,
        boardsStatus: boardsStatus
      },
      () => {
        if (this.state.trainingFlags[index]) {
          this.trainingProcessForBoard(index);
        }
      }
    );
  };

  trainingProcessForBoard = async index => {
    let playerXnnAi = this.state.playerXnnAis[index];
    let playerOnnAi = this.state.playerOnnAis[index];
    let xETotals = this.state.xETotals;
    let oETotals = this.state.oETotals;
    let boards = this.state.boards;
    let board = boards[index];
    let trainingFlags = this.state.trainingFlags;
    let trainingFlag = trainingFlags[index];

    while (board.winner === "" && trainingFlag) {
      trainingFlag = this.state.trainingFlags[index];
      let boardsStatus = this.state.boardsStatus;
      let boardStatus = boardsStatus[index];

      board = playerXnnAi.train(board);
      xETotals[index] = playerXnnAi.Etotal;
      boards[index] = board;
      for (let j = 0; j < boardSize; j++) {
        boardStatus[j] = board.getSlotStatus(j);
      }
      await this.setStateAsync({
        boards: boards,
        boardsStatus: boardsStatus,
        xETotals: xETotals
      });

      board = playerOnnAi.train(board);
      oETotals[index] = playerOnnAi.Etotal;
      boards[index] = board;
      for (let j = 0; j < boardSize; j++) {
        boardStatus[j] = board.getSlotStatus(j);
      }
      await this.setStateAsync({
        boards: boards,
        boardsStatus: boardsStatus,
        oETotals: oETotals
      });
    }

    if (trainingFlag) {
      let trainingGameCount = this.state.trainingGameCount;
      let playerXWinningCount = this.state.playerXWinningCount;
      let playerOWinningCount = this.state.playerOWinningCount;
      let drawCount = this.state.drawCount;

      trainingGameCount[index]++;
      if (board.winner === "X") {
        playerXWinningCount[index]++;
      } else if (board.winner === "O") {
        playerOWinningCount[index]++;
      } else if (board.winner === "_") {
        drawCount[index]++;
      }
      this.setState(
        {
          trainingGameCount: trainingGameCount,
          playerXWinningCount: playerXWinningCount,
          playerOWinningCount: playerOWinningCount,
          drawCount: drawCount
        },
        () => {
          if (this.state.trainingFlags[index]) {
            this.trainIntervalTimer = setTimeout(() => {
              this.onTrainButtonClick(index, true);
            }, 1000);
          }
        }
      );
    }
    // trainingFlags[index] = false;
    // this.setState({ trainingFlags: trainingFlags });
  };

  nnAiPlayGame = () => {
    console.log("[nnAiPlayGame]: ", this.state.gameBoard.winner);
    if (this.state.gameBoard.winner === "") {
      if (this.state.gameBoard.turn === this.state.gameBoardnnAiSymbol) {
        let nextMove = this.state.gameBoardnnAi.play(this.state.gameBoard);
        this.state.gameBoard.placeAtSlot(nextMove);
        let boardStatus = [];
        for (let j = 0; j < boardSize; j++) {
          boardStatus[j] = this.state.gameBoard.getSlotStatus(j);
        }
        this.setState({ gameBoardStatus: boardStatus });
      }
    } else {
      this.setState({ gameBoardStarted: false });
    }
  };

  setStateAsync = state => {
    return new Promise(resolve => {
      this.setState(state, () => {
        setTimeout(resolve, 100);
      });
    });
  };

  render() {
    return (
      <Layout>
        <Header className="header">
          <Menu
            theme="dark"
            mode="horizontal"
            style={{ lineHeight: "64px" }}
            selectedKeys={[this.state.selectedMenuKey]}
            onSelect={selectedItemInfo => {
              console.log("[onSelect]: ", selectedItemInfo);
              this.setState({ selectedMenuKey: selectedItemInfo.key });
            }}
          >
            <Menu.Item key="train">Train</Menu.Item>
            <Menu.Item key="play">Play</Menu.Item>
          </Menu>
        </Header>

        {this.state.selectedMenuKey === "train"
          ? this.renderBoards()
          : this.renderGame()}
      </Layout>
    );
  }

  renderBoards = () => {
    if (this.state.boardCount > 0) {
      return (
        <Content style={{ background: "#fff", margin: 0, minHeight: 280 }}>
          <Row type="flex" justify="space-around">
            {Array(this.state.boardCount)
              .fill()
              .map((_, i) => {
                //let board = this.state.boards[i];
                let boardStatus = this.state.boardsStatus[i];
                //console.log("[boardStatus]: ", boardStatus);
                return (
                  <Col
                    key={`${i}`}
                    style={{
                      background: "#B0BEC5",
                      margin: "1px",
                      height: boardHeight + "px",
                      width: boardWidth + "px"
                    }}
                  >
                    <Stage width={boardWidth} height={boardHeight}>
                      <Layer>
                        <Line
                          points={[
                            boardWidth / 3,
                            0,
                            boardWidth / 3,
                            boardHeight
                          ]}
                          stroke="black"
                          strokeWidth={1}
                        />
                        <Line
                          points={[
                            boardWidth / 3 * 2,
                            0,
                            boardWidth / 3 * 2,
                            boardHeight
                          ]}
                          stroke="black"
                          strokeWidth={1}
                        />
                        <Line
                          points={[
                            0,
                            boardHeight / 3,
                            boardWidth,
                            boardHeight / 3
                          ]}
                          stroke="black"
                          strokeWidth={1}
                        />
                        <Line
                          points={[
                            0,
                            boardHeight / 3 * 2,
                            boardWidth,
                            boardHeight / 3 * 2
                          ]}
                          stroke="black"
                          strokeWidth={1}
                        />
                        {boardStatus.map((slotStatus, j) => {
                          let columnIndex = j % 3;
                          let rowIndex = (j - columnIndex) / 3;
                          let xPos = columnIndex * slotWidth + halfSlotWidth;
                          let yPos = rowIndex * slotHeight + halfSlotHeight;
                          if (slotStatus === "X") {
                            let line1Points = [
                              xPos - quaterSlotWidth,
                              yPos - quaterSlotHeight,
                              xPos + quaterSlotWidth,
                              yPos + quaterSlotHeight
                            ];
                            let line2Points = [
                              xPos + quaterSlotWidth,
                              yPos - quaterSlotHeight,
                              xPos - quaterSlotWidth,
                              yPos + quaterSlotHeight
                            ];
                            //console.log("[line1Points]: ", line1Points);
                            //console.log("[line2Points]: ", line2Points);
                            return [
                              <Line
                                key={"x1" + j}
                                points={line1Points}
                                stroke={"green"}
                              />,
                              <Line
                                key={"x2" + j}
                                points={line2Points}
                                stroke={"green"}
                              />
                            ];
                          } else if (slotStatus === "O") {
                            return (
                              <Circle
                                key={"shape" + j}
                                x={xPos}
                                y={yPos}
                                width={halfSlotWidth}
                                height={halfSlotHeight}
                                stroke={"red"}
                              />
                            );
                          } else {
                            return null;
                          }
                        })}
                        <Rect
                          x={0}
                          y={0}
                          width={boardWidth}
                          height={boardHeight}
                          onClick={e => {
                            let xPos = e.evt.layerX;
                            let yPos = e.evt.layerY;
                            let boardsStatus = this.state.boardsStatus;
                            let boardStatus = boardsStatus[i];
                            let columnIndex = Math.floor(xPos / slotWidth);
                            let rowIndex = Math.floor(yPos / slotHeight);
                            console.log(
                              "[onClick]: ",
                              xPos,
                              yPos,
                              columnIndex,
                              rowIndex
                            );
                            this.state.boards[i].placeAtSlot(
                              boardColumnCount * rowIndex + columnIndex
                            );
                            for (let j = 0; j < boardSize; j++) {
                              boardStatus[j] = this.state.boards[
                                i
                              ].getSlotStatus(j);
                            }
                            boardsStatus[i] = boardStatus;
                            this.setState({ boardsStatus: boardsStatus });
                          }}
                        />
                      </Layer>
                    </Stage>
                  </Col>
                );
              })}
          </Row>
          <Row type="flex" justify="space-around">
            {Array(this.state.boardCount)
              .fill()
              .map((_, i) => {
                // let board = this.state.boards[i];
                // let boardStatus = this.state.boardsStatus[i];
                return (
                  <Col key={"" + i}>
                    <Button
                      type="primary"
                      disabled={this.state.trainingFlags[i]}
                      onClick={() => {
                        this.onStartButtonClick(i);
                      }}
                    >
                      {"Init"}
                    </Button>
                    <Button
                      type="default"
                      onClick={() => {
                        this.onTrainButtonClick(i);
                      }}
                    >
                      {this.state.trainingFlags[i] ? "Stop" : "Train"}
                    </Button>
                  </Col>
                );
              })}
          </Row>
          <Row type="flex" justify="space-around">
            {Array(this.state.boardCount)
              .fill()
              .map((_, i) => {
                let board = this.state.boards[i];
                return (
                  <Col key={"" + i}>
                    <span style={{ padding: "3px" }}>{`Winner: ${
                      board.winner === "_" ? "draw" : board.winner
                    }`}</span>
                    <br />
                    <span style={{ padding: "3px" }}>{`Game count: ${
                      this.state.trainingGameCount[i]
                    }`}</span>
                    <br />
                    <span style={{ padding: "3px" }}>{`X win count: ${
                      this.state.playerXWinningCount[i]
                    }`}</span>
                    <br />
                    <span style={{ padding: "3px" }}>{`O win count: ${
                      this.state.playerOWinningCount[i]
                    }`}</span>
                    <br />
                    <span style={{ padding: "3px" }}>{`Draw count: ${
                      this.state.drawCount[i]
                    }`}</span>
                    <br />
                    <span style={{ padding: "3px" }}>{`X Error: ${
                      this.state.xETotals[i]
                    }`}</span>
                    <br />
                    <span style={{ padding: "3px" }}>{`O Error: ${
                      this.state.oETotals[i]
                    }`}</span>
                  </Col>
                );
              })}
          </Row>
          <Row type="flex" justify="space-around">
            {Array(this.state.boardCount)
              .fill()
              .map((_, i) => {
                return (
                  <Col key={"" + i}>
                    <label>{"Player X weights:"}</label>
                    <Input.TextArea
                      rows={4}
                      value={JSON.stringify(
                        this.state.playerXnnAis[i].getNeuralNetworkWeightsInfo()
                      )}
                    />
                  </Col>
                );
              })}
          </Row>
          <Row type="flex" justify="space-around">
            {Array(this.state.boardCount)
              .fill()
              .map((_, i) => {
                return (
                  <Col key={"" + i}>
                    <label>{"Player O weights:"}</label>
                    <Input.TextArea
                      rows={4}
                      value={JSON.stringify(
                        this.state.playerOnnAis[i].getNeuralNetworkWeightsInfo()
                      )}
                    />
                  </Col>
                );
              })}
          </Row>
        </Content>
      );
    } else {
      return null;
    }
  };

  renderGame = () => {
    return (
      <Content style={{ background: "#fff", margin: 0, minHeight: 280 }}>
        <Row type="flex" justify="space-around">
          <Col
            style={{
              background: "#B0BEC5",
              margin: "1px",
              height: boardHeight + "px",
              weight: boardWidth + "px"
            }}
          >
            <Stage width={boardWidth} height={boardHeight}>
              <Layer>
                <Line
                  points={[boardWidth / 3, 0, boardWidth / 3, boardHeight]}
                  stroke="black"
                  strokeWidth={1}
                />
                <Line
                  points={[
                    boardWidth / 3 * 2,
                    0,
                    boardWidth / 3 * 2,
                    boardHeight
                  ]}
                  stroke="black"
                  strokeWidth={1}
                />
                <Line
                  points={[0, boardHeight / 3, boardWidth, boardHeight / 3]}
                  stroke="black"
                  strokeWidth={1}
                />
                <Line
                  points={[
                    0,
                    boardHeight / 3 * 2,
                    boardWidth,
                    boardHeight / 3 * 2
                  ]}
                  stroke="black"
                  strokeWidth={1}
                />
                {this.state.gameBoardStatus.map((slotStatus, j) => {
                  let columnIndex = j % 3;
                  let rowIndex = (j - columnIndex) / 3;
                  let xPos = columnIndex * slotWidth + halfSlotWidth;
                  let yPos = rowIndex * slotHeight + halfSlotHeight;
                  if (slotStatus === "X") {
                    let line1Points = [
                      xPos - quaterSlotWidth,
                      yPos - quaterSlotHeight,
                      xPos + quaterSlotWidth,
                      yPos + quaterSlotHeight
                    ];
                    let line2Points = [
                      xPos + quaterSlotWidth,
                      yPos - quaterSlotHeight,
                      xPos - quaterSlotWidth,
                      yPos + quaterSlotHeight
                    ];
                    //console.log("[line1Points]: ", line1Points);
                    //console.log("[line2Points]: ", line2Points);
                    return [
                      <Line
                        key={"x1" + j}
                        points={line1Points}
                        stroke={"green"}
                      />,
                      <Line
                        key={"x2" + j}
                        points={line2Points}
                        stroke={"green"}
                      />
                    ];
                  } else if (slotStatus === "O") {
                    return (
                      <Circle
                        key={"shape" + j}
                        x={xPos}
                        y={yPos}
                        width={halfSlotWidth}
                        height={halfSlotHeight}
                        stroke={"red"}
                      />
                    );
                  } else {
                    return null;
                  }
                })}
                <Rect
                  x={0}
                  y={0}
                  width={boardWidth}
                  height={boardHeight}
                  onClick={e => {
                    console.log(
                      "onClick: ",
                      this.state.gameBoardStarted,
                      this.state.gameBoard.turn,
                      this.state.gameBoardnnAiSymbol
                    );
                    if (this.state.gameBoardStarted) {
                      if (
                        this.state.gameBoard.turn !==
                        this.state.gameBoardnnAiSymbol
                      ) {
                        let xPos = e.evt.layerX;
                        let yPos = e.evt.layerY;
                        let boardStatus = this.state.gameBoardStatus;
                        let columnIndex = Math.floor(xPos / slotWidth);
                        let rowIndex = Math.floor(yPos / slotHeight);
                        console.log(
                          "[onClick]: ",
                          xPos,
                          yPos,
                          columnIndex,
                          rowIndex
                        );

                        // let winners = this.state.gameBoard.evalueateMoveNew(
                        //   this.state.gameBoard.board,
                        //   this.state.gameBoardnnAiSymbol === "X" ? "O" : "X",
                        //   []
                        // );
                        // console.log("[winners]: ", winners);

                        this.state.gameBoard.placeAtSlot(
                          boardColumnCount * rowIndex + columnIndex
                        );

                        for (let j = 0; j < boardSize; j++) {
                          boardStatus[j] = this.state.gameBoard.getSlotStatus(
                            j
                          );
                        }
                        this.setState(
                          {
                            gameBoardStatus: boardStatus
                          },
                          this.nnAiPlayGame
                        );
                      }
                    }
                  }}
                />
              </Layer>
            </Stage>
          </Col>
        </Row>
        <Row type="flex" justify="space-around">
          <Col>
            <h3>{`${this.state.gameBoard.turn}'s turn | Winner: ${
              this.state.gameBoard.winner === "_"
                ? "draw"
                : this.state.gameBoard.winner
            }`}</h3>
            <Button
              type="primary"
              style={{ width: "100%" }}
              onClick={() => {
                if (this.state.gameBoardStarted) {
                  this.setState({
                    gameBoardStarted: false
                  });
                } else {
                  let gameBoardStatus = [];
                  for (let j = 0; j < boardSize; j++) {
                    gameBoardStatus[j] = "";
                  }
                  this.setState(
                    {
                      gameBoard: new TicTacToeGame(),
                      gameBoardnnAi: new NeuralNetworkAI(
                        this.state.gameBoardnnAiSymbol,
                        neuralNetworkInputsCount,
                        neuralNetworkOutputsCount,
                        neuralNetworkHiddenLayersCount,
                        this.state.gameBoardWeights
                      ),
                      gameBoardStatus: gameBoardStatus,
                      gameBoardStarted: true
                    },
                    this.nnAiPlayGame
                  );
                }
              }}
            >
              {this.state.gameBoardStarted ? "Stop" : "Start"}
            </Button>
          </Col>
        </Row>
        <Row type="flex" justify="space-around">
          <Col>
            <label style={{ margin: "1em", fontWeight: "bold" }}>
              {"AI Symbol"}
            </label>
            <Dropdown
              placement="bottomCenter"
              disabled={this.state.gameBoardStarted}
              overlay={
                <Menu
                  onClick={itemInfo => {
                    this.setState({ gameBoardnnAiSymbol: itemInfo.key });
                  }}
                >
                  <Menu.Item key="X">{"X"}</Menu.Item>
                  <Menu.Item key="O">{"O"}</Menu.Item>
                </Menu>
              }
            >
              <Button style={{ margin: "1em" }}>
                {this.state.gameBoardnnAiSymbol}
              </Button>
            </Dropdown>
          </Col>
        </Row>
        <Row type="flex" justify="space-around">
          <Col>
            <label style={{ fontWeight: "bold" }}>{"AI weights:"}</label>
            <Input.TextArea
              rows={4}
              disabled={this.state.gameBoardStarted}
              defaultValue={JSON.stringify(this.state.gameBoardWeights)}
              onChange={(e, v) => {
                this.setState({ gameBoardWeights: e.target.value });
              }}
            />
          </Col>
        </Row>
      </Content>
    );
  };
}

export default App;
